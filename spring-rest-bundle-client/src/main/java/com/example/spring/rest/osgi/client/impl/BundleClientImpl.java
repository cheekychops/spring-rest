package com.example.spring.rest.osgi.client.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

import org.codehaus.jackson.map.ObjectMapper;

import com.example.spring.rest.osgi.client.BundleClient;
import com.example.spring.rest.osgi.dto.BundleDTO;


public final class BundleClientImpl implements BundleClient {
	private static final String BASE_URL = "http://localhost:8181";
	private static final String CONTEXT = "/spring-rest";

	public BundleDTO getBundle(long bundleId) throws IOException {
        URL url = new URL(BASE_URL + CONTEXT + "/bundle/" + bundleId);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(url.openStream(), BundleDTO.class);
	}

	@Override
	public String executeCommand(String command) throws MalformedURLException  {
        URL url = new URL(BASE_URL + CONTEXT + "/shell?command=" + URLEncoder.encode(command));
        InputStream is;
		try {
			is = (InputStream) url.getContent();
	        return getContentAsString(is);
		} catch (IOException e) {
			return "Output stream terminated! "+e.toString();
		}
	}

	private String getContentAsString(InputStream is) { 
		Scanner s = new Scanner(is).useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}

}