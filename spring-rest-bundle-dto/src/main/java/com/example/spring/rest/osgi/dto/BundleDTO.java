package com.example.spring.rest.osgi.dto;

import org.osgi.framework.Bundle;

public class BundleDTO {

	private long id;
	private String symbolicName;

	private BundleDTO() {
	}

	private BundleDTO(long id, String symbolicName) {
		this.id = id;
		this.symbolicName = symbolicName;
	}

	public long getId() {
		return id;
	}

	public String getSymbolicName() {
		return symbolicName;
	}

	public static BundleDTO valueOf(Bundle bundle) {
		return new BundleDTO(bundle.getBundleId(), bundle.getSymbolicName());
	}

	public static BundleDTO[] valueOf(Bundle[] bundles) {
		BundleDTO[] result = new BundleDTO[bundles.length];
		for (int i = 0; i< bundles.length; i++) {
			result[i] = valueOf(bundles[i]);
		}
		return result;
	}
	
	

}
