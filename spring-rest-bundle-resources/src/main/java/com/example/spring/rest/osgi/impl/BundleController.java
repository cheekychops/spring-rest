package com.example.spring.rest.osgi.impl;

import org.osgi.framework.BundleContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.spring.rest.osgi.dto.BundleDTO;

@Controller
public class BundleController {

	BundleContext bundleContext;

	@Autowired
	public BundleController(BundleContext bundleContext) {
		this.bundleContext = bundleContext;
	}

	@RequestMapping("bundle/{id}")
	@ResponseBody
	public BundleDTO getById(@PathVariable Long id) {
		return BundleDTO.valueOf(bundleContext.getBundle(id));
    }

	@RequestMapping("bundles")
	@ResponseBody
	public BundleDTO[] getAll() {
		return BundleDTO.valueOf(bundleContext.getBundles());
    }

}
