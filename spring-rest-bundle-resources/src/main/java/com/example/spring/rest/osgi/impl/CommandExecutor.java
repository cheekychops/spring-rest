package com.example.spring.rest.osgi.impl;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

import org.apache.felix.service.command.CommandProcessor;
import org.apache.felix.service.command.CommandSession;

public class CommandExecutor {

	private final Executor executor = Executors.newSingleThreadExecutor();
	private final CommandProcessor commandProcessor;
	private final long timeout;

	public CommandExecutor(CommandProcessor commandProcessor, long timeout) {
		super();
		this.commandProcessor = commandProcessor;
		this.timeout = timeout;
	}

	public String execute(final String... commands) {
		String response;
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		final PrintStream printStream = new PrintStream(byteArrayOutputStream);
		final CommandSession commandSession = commandProcessor.createSession(
				System.in, printStream, System.err);
		FutureTask<String> commandFuture = new FutureTask<String>(
				new Callable<String>() {
					public String call() {
						try {
							for (String command : commands) {
								System.err.println(command);
								commandSession.execute(command);
							}
						} catch (Exception e) {
							e.printStackTrace(System.err);
						} finally {
							commandSession.close();
						}
						return byteArrayOutputStream.toString();
					}
				});

		try {
			executor.execute(commandFuture);
			response = commandFuture.get(timeout, TimeUnit.MILLISECONDS);
		} catch (Exception e) {
			e.printStackTrace(System.err);
			response = "SHELL COMMAND TIMED OUT: "+e;
		}

		System.out.println(response);
		return response;
	}
}
