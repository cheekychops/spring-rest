package com.example.spring.rest.osgi.impl;

import org.apache.felix.service.command.CommandProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ShellController {

	private final CommandExecutor commandExecutor;

	@Autowired
	public ShellController(CommandProcessor commandProcessor) {
		this.commandExecutor = new CommandExecutor(commandProcessor, 120000L);
	}

	@RequestMapping("shell")
	@ResponseBody
	public String executeCommand(@RequestParam(value="command", required=true) String command) {
		return commandExecutor.execute(command);
    }
}
