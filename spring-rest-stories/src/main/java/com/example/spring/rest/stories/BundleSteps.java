package com.example.spring.rest.stories;

import java.io.IOException;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.example.spring.rest.osgi.client.BundleClient;
import com.example.spring.rest.osgi.dto.BundleDTO;
import static org.junit.Assert.*;

public class BundleSteps {

	private long bundleId;
	private final BundleClient bundleClient;
	private BundleDTO bundle;

	public BundleSteps(BundleClient bundleClient) {
		this.bundleClient = bundleClient;
	}
	
	@Given("bundle id $bundleId")
	public void givenBundleId(long bundleId) {
		this.bundleId = bundleId;
	}
	
	@When("the bundle is retrieved")
	public void whenTheBundleNameIsRetrieved() throws IOException {
		bundle = bundleClient.getBundle(bundleId);
	}
	
	@Then("the description should be $name")
	public void thenTheDescriptionShouldBe(String name) {
		assertEquals(bundle.getSymbolicName(), name);
	}
}
