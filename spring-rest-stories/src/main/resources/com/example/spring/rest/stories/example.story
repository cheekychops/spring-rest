Narrative:
In order to demonstrate a working test
As a story writer
I want to make this first test pass

Scenario: Retrieve the first bundle from the REST API

Given bundle id 0
When the bundle is retrieved
Then the description should be org.apache.felix.framework