package com.example.spring.rest.osgi;

import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.configureConsole;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.karafDistributionConfiguration;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.logLevel;

import java.io.File;

import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.karaf.options.LogLevelOption.LogLevel;

@RunWith(PaxExam.class)
public abstract class AbstractFuseIntegrationTest extends AbstractKarafIntegrationTest {

	@Configuration
	@Override
	protected Option[] configuration() {
		return options(
				karafDistributionConfiguration()
					.name("JBoss Fuse")
					.frameworkUrl("mvn:org.jboss.fuse/jboss-fuse/6.0.0.redhat-024/zip")
					.karafVersion("2.3.0.redhat-600024")
					.unpackDirectory(new File("target/pax"))
					.useDeployFolder(false),
//				new KeepRuntimeFolderOption(),
				logLevel(LogLevel.INFO),
				configureConsole().ignoreLocalConsole());
	}
}
