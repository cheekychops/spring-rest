package com.example.spring.rest.osgi;

import static org.ops4j.pax.exam.CoreOptions.maven;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.karafDistributionConfiguration;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.logLevel;

import java.io.File;

import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.karaf.options.KarafDistributionOption;
import org.ops4j.pax.exam.karaf.options.LogLevelOption.LogLevel;
import org.ops4j.pax.exam.options.MavenArtifactUrlReference;
import org.ops4j.pax.exam.options.MavenUrlReference;

@RunWith(PaxExam.class)
public abstract class AbstractKaraf3IntegrationTest extends AbstractKarafIntegrationTest {

	@Configuration
	@Override
	public Option[] configuration() {
        MavenArtifactUrlReference karafUrl = maven()
                .groupId("org.apache.karaf")
                .artifactId("apache-karaf")
                .version("3.0.0")
                .type("tar.gz");
            MavenUrlReference karafStandardRepo = maven()
                .groupId("org.apache.karaf.features")
                .artifactId("enterprise")
                .classifier("features")
                .type("xml")
                .version("3.0.0");
            return new Option[] {
                // KarafDistributionOption.debugConfiguration("5005", true),
                karafDistributionConfiguration()
                    .frameworkUrl(karafUrl)
                    .unpackDirectory(new File("target/exam"))
                    .useDeployFolder(false),
                KarafDistributionOption.features(karafStandardRepo , "scr"),
				logLevel(LogLevel.INFO)
           };
//		return options(
//				karafDistributionConfiguration()
//					.name("Apache Karaf")
//					.frameworkUrl("mvn:org.apache.karaf/apache-karaf/3.0.0/tar.gz")
//					.karafVersion("3.0.0")
//					.unpackDirectory(new File("target/pax"))
//					.useDeployFolder(false),
//				logLevel(LogLevel.INFO),
//				configureConsole().ignoreLocalConsole());
	}
}
