package com.example.spring.rest.osgi;

import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.configureConsole;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.karafDistributionConfiguration;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.logLevel;

import java.io.File;

import javax.inject.Inject;

import org.apache.felix.service.command.CommandProcessor;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.ProbeBuilder;
import org.ops4j.pax.exam.TestProbeBuilder;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.karaf.options.LogLevelOption.LogLevel;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;
import org.osgi.framework.Constants;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public abstract class AbstractKarafIntegrationTest {

	private static final long COMMAND_TIMEOUT = 60000L;

	@Inject
	private CommandProcessor commandProcessor;

	protected CommandExecutor shell;

	@Before
	public void before() {
		shell = new CommandExecutor(commandProcessor, COMMAND_TIMEOUT);	
	}
	
	protected Option[] combine(Option[] options1, Option[] options2) {
		Option[] result = new Option[options1.length + options2.length];
		System.arraycopy(options1, 0, result, 0, options1.length);
		System.arraycopy(options2, 0, result, options1.length, options2.length);
		return result;
	}

	@Configuration
	protected Option[] configuration() {
		return options(
				karafDistributionConfiguration()
					.name("Apache Karaf")
					.frameworkUrl("mvn:org.apache.karaf/apache-karaf/2.3.0/tar.gz")
					.karafVersion("2.3.0")
					.unpackDirectory(new File("target/pax"))
					.useDeployFolder(false),
				logLevel(LogLevel.INFO),
				configureConsole().ignoreLocalConsole());
	}

	@ProbeBuilder
	public TestProbeBuilder probeConfiguration(TestProbeBuilder probe) {
		probe.setHeader(Constants.DYNAMICIMPORT_PACKAGE, "*,org.apache.felix.service.command;status=provisional");
		return probe;
	}
}
