package com.example.spring.rest.osgi;

import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.configureConsole;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.features;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.karafDistributionConfiguration;

import java.io.File;
import java.io.IOException;

import org.ops4j.pax.exam.ExamSystem;
import org.ops4j.pax.exam.TestContainer;
import org.ops4j.pax.exam.spi.PaxExamRuntime;

import com.example.spring.rest.osgi.client.BundleClient;
import com.example.spring.rest.osgi.client.impl.BundleClientImpl;
import com.example.spring.rest.osgi.dto.BundleDTO;

public abstract class AbstractServer {
	private TestContainer container;
	private BundleClient bundleClient;
	private boolean running;
	
	protected AbstractServer() {
		this.bundleClient = new BundleClientImpl();
	}
	
	public ExamSystem createSystem() throws IOException {
		ExamSystem system = PaxExamRuntime.createServerSystem(
			karafDistributionConfiguration()
				.name(getName())
				.frameworkUrl(getFrameworkUrl())
				.karafVersion(getKarafVersion())
				.unpackDirectory(new File("target/pax"))
				.useDeployFolder(false),
//			logLevel(LogLevel.INFO),
			configureConsole().ignoreLocalConsole(),
			features("mvn:com.example/spring-rest-features/0.0.1-SNAPSHOT/xml/features", "spring-rest")
		);
		return system;
	}
	
	public void start(int timeoutInSeconds) throws IOException, InterruptedException {
		start();
		waitForStartup(timeoutInSeconds);
	}

	public void start() throws IOException {
		container = PaxExamRuntime.createContainer(createSystem());
		container.start();
	}
	
	public void waitForStartup(int timeoutInSeconds) throws InterruptedException {
		long time = System.currentTimeMillis();
		long endTime = time + (timeoutInSeconds * 1000);
		while (time < endTime){
			if (isStarted()) {
				running = true;
				System.out.println("READY!");
				break;
			} else {
				Thread.sleep(1000L);
			}
		}
	}
	
	private boolean isStarted() {
		try {
			BundleDTO bundle = bundleClient.getBundle(0L);
			return bundle.getId() == 0L;
		} catch (Exception e) {
			return false;
		}
	}

	public void stop() {
		container.stop();
	}

	public boolean isRunning() {
		return running;
	}
	
	public String executeCommand(String command) throws IOException {
		return bundleClient.executeCommand(command);
	}

	public abstract String getName();
	public abstract String getFrameworkUrl();
	public abstract String getKarafVersion();
}
