package com.example.spring.rest.osgi;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.features;

import java.io.IOException;

import javax.inject.Inject;

import org.junit.Ignore;
import org.junit.Test;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.util.Filter;
import org.springframework.web.context.WebApplicationContext;

import com.example.spring.rest.osgi.client.BundleClient;
import com.example.spring.rest.osgi.dto.BundleDTO;

public class BundleClientIT extends AbstractFuseIntegrationTest {

	@Inject
	private BundleClient bundleClient;
	
	@Inject
	@Filter("org.springframework.context.service.name=com.example.spring-rest")
	@SuppressWarnings("unused")
	//This ensures that the web app is fully initialized before tests are run
	private WebApplicationContext webApplicationContext;

	@Configuration
	@Override
	public Option[] configuration() {
		Option[] superOptions = super.configuration();
		Option[] myOptions = new Option[] {
			features("mvn:com.example/spring-rest-features/0.0.1-SNAPSHOT/xml/features",
					"spring-rest-client", 
					"spring-rest")
		};
		return combine(superOptions, myOptions);
	}

	@Test
	public void shouldReturnFirstBundle() throws IOException, InterruptedException {
		BundleDTO bundle = bundleClient.getBundle(0L);
		assertThat(bundle.getId(), is(0L));
		assertThat(bundle.getSymbolicName(), is("org.apache.felix.framework"));
	}

	@Test
	public void shouldExecuteCommands() throws IOException, InterruptedException {
		String output = bundleClient.executeCommand("features:list | grep \"installed  \"");
		assertThat(output, containsString("spring-rest"));
		assertThat(output, containsString("spring-rest-client"));
	}
	
	@Test
	public void shouldCreateFabricEnsemble() throws IOException, InterruptedException {
		String output = bundleClient.executeCommand("fabric:create --clean");
		assertThat(output, containsString("zookeeper"));
		output = bundleClient.executeCommand("fabric:container-list");
		assertThat(output, containsString("root"));
		output = bundleClient.executeCommand("sleep 30000");
		output = bundleClient.executeCommand("features:list | grep \"installed  \"");
		assertThat(output, containsString("spring-rest"));
		assertThat(output, containsString("spring-rest-client"));
	}

	@Test
	@Ignore
	public void shouldDeployFabricApplication() throws IOException, InterruptedException {
		String output = bundleClient.executeCommand("shell:source mvn:com.example/spring-rest-features/0.0.1-SNAPSHOT/karaf-create/container");
		assertThat(output, containsString("zookeeper"));
		output = bundleClient.executeCommand("fabric:container-list");
		assertThat(output, containsString("root"));
		assertThat(output, containsString("spring-rest"));
		output = bundleClient.executeCommand("sleep 30000");
		output = bundleClient.executeCommand("features:list | grep \"installed  \"");
		assertThat(output, containsString("spring-rest"));
		assertThat(output, containsString("spring-rest-client"));
	}

}
