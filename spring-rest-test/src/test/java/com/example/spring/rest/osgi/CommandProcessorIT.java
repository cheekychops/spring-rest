package com.example.spring.rest.osgi;

import org.junit.Test;

public class CommandProcessorIT extends AbstractFuseIntegrationTest {

	@Test
	public void testFeatures() throws Exception {
		shell.execute("features:addurl mvn:com.example/spring-rest-features/0.0.1-SNAPSHOT/xml/features");
		shell.execute("features:install spring-web");
		shell.execute("features:install spring-dm");
		shell.execute("features:install spring-dm-web");
		shell.execute("features:install war");

		shell.execute("features:install spring-rest");
		shell.execute("features:install spring-rest-client");
		shell.execute("features:list | grep -i spring-rest");
		shell.execute("osgi:list | grep -i spring-rest");

	}
}
