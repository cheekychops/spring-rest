package com.example.spring.rest.osgi;

public class FuseServer extends AbstractServer {

	@Override
	public String getName() {
		return "JBoss Fuse";
	}

	@Override
	public String getFrameworkUrl() {
		return "mvn:org.jboss.fuse/jboss-fuse/6.0.0.redhat-024/zip";
	}

	@Override
	public String getKarafVersion() {
		return "2.3.0.redhat-600024";
	}

}
