package com.example.spring.rest.osgi;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

public class FuseServerTest {

	private static FuseServer server;
	
	@BeforeClass
	public static void beforeClass() throws IOException, InterruptedException {
		server = new FuseServer();
		server.start(120);		
	}

	@Test
	public void shouldBeStarted() {
		assertEquals(server.isRunning(), true);
	}

	@Test
	public void shouldExecuteShellCommands() throws IOException {
		String output = server.executeCommand("features:list | grep \"installed  \"");
		System.err.println("Output: "+output);
	}

	@Test
	public void shouldDeployFabricApplication() throws IOException, InterruptedException {
		String output = server.executeCommand("shell:source mvn:com.example/spring-rest-features/0.0.1-SNAPSHOT/karaf-create/container");
		System.err.println("Output: "+output);
		server.waitForStartup(120);
		output = server.executeCommand("fabric:container-list");
		System.err.println("Output: "+output);
		output = server.executeCommand("features:list | grep \"installed  \"");
		System.err.println("Output: "+output);
	}

}
