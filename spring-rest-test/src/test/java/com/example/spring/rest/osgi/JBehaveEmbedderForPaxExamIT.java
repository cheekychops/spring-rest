package com.example.spring.rest.osgi;

import static org.hamcrest.core.Is.is;
import static org.jbehave.core.reporters.Format.CONSOLE;
import static org.jbehave.core.reporters.Format.HTML;
import static org.jbehave.core.reporters.Format.TXT;
import static org.jbehave.core.reporters.Format.XML;
import static org.junit.Assert.assertThat;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.features;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.embedder.EmbedderControls;
import org.jbehave.core.reporters.CrossReference;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.jbehave.osgi.core.configuration.ConfigurationOsgi;
import org.jbehave.osgi.core.embedder.EmbedderOsgi;
import org.jbehave.osgi.core.io.StoryFinderOsgi;
import org.jbehave.osgi.core.reporters.StoryReporterBuilderOsgi;
import org.junit.Ignore;
import org.junit.Test;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.util.Filter;
import org.springframework.web.context.WebApplicationContext;

import com.example.spring.rest.osgi.client.BundleClient;
import com.example.spring.rest.osgi.dto.BundleDTO;
import com.example.spring.rest.stories.BeforeAfterSteps;
import com.example.spring.rest.stories.BundleSteps;

public class JBehaveEmbedderForPaxExamIT extends AbstractKaraf3IntegrationTest {

	@Inject
	private BundleClient bundleClient;

	@Inject
	@Filter("org.springframework.context.service.name=com.example.spring-rest")
	@SuppressWarnings("unused")
	// This ensures that the web app is fully initialized before tests are run
	private WebApplicationContext webApplicationContext;

	@org.ops4j.pax.exam.Configuration
	@Override
	public Option[] configuration() {
		Option[] superOptions = super.configuration();
		Option[] myOptions = options(
				features("mvn:com.example/spring-rest-features/0.0.1-SNAPSHOT/xml/features",
						"spring-rest-client", 
						"spring-rest",
						"jbehave-osgi",
						"spring-rest-stories"
						)
		);
		return combine(superOptions, myOptions);
	}

	@Test
	public void shouldReturnFirstBundle() throws IOException, InterruptedException {
		BundleDTO bundle = bundleClient.getBundle(0L);
		assertThat(bundle.getId(), is(0L));
		assertThat(bundle.getSymbolicName(), is("org.apache.felix.framework"));
	}

	@Test
	@Ignore
	public void shouldRunStories() throws Exception {

		List<String> storyPaths = new StoryFinderOsgi(BeforeAfterSteps.class)
				.findPaths("/com/example/spring/rest/stories",
						new String[] { "*.story" },
						new String[] { });

		EmbedderOsgi embedderOsgi = new EmbedderOsgi(BeforeAfterSteps.class);
		embedderOsgi.useConfiguration(configuration(BeforeAfterSteps.class));
		embedderOsgi.useStepsFactory(stepsFactory(BeforeAfterSteps.class));
		embedderOsgi.useEmbedderControls(embedderControls());
		
		embedderOsgi.runStoriesAsPaths(storyPaths);
	}

	private EmbedderControls embedderControls() {
		return new EmbedderControls().doIgnoreFailureInStories(true)
				.doIgnoreFailureInView(true);
	}

	private Configuration configuration(Class<?> testClass) {
		return new ConfigurationOsgi(testClass)
				.useStoryReporterBuilder(
						new StoryReporterBuilderOsgi(testClass)
								.withDefaultFormats()
								.withFormats(CONSOLE, TXT, HTML, XML)
								.withCrossReference(new CrossReference()));
	}

	private InjectableStepsFactory stepsFactory(Class<?> testClass) {
		return new InstanceStepsFactory(configuration(testClass),
				new BundleSteps(bundleClient), new BeforeAfterSteps());
	}

}