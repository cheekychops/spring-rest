package com.example.spring.rest.osgi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.junit.PaxExam;

@RunWith(PaxExam.class)
public class KarafIT extends AbstractKarafIntegrationTest {

	@Test
	public void testShell() throws Exception {
		shell.execute("osgi:list");
	}
}