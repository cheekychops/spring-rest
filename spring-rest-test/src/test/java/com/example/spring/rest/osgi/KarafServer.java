package com.example.spring.rest.osgi;

public class KarafServer extends AbstractServer {

	@Override
	public String getName() {
		return "Apache Karaf";
	}

	@Override
	public String getFrameworkUrl() {
		return "mvn:org.apache.karaf/apache-karaf/" + getKarafVersion() + "/tar.gz";
	}

	@Override
	public String getKarafVersion() {
		return "2.3.0.redhat-60024";
	}
}
